import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class APIServiceService {

    constructor(private httpClient: HttpClient) { }
   public getContacts() {
        return this.httpClient.
            get('https://jsonplaceholder.typicode.com/users');
    }
}
