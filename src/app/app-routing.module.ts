import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContatsComponent } from './contats/contats.component';
import { CreateContactComponent } from './create-contact/create-contact.component';
import { CreateProduitComponent } from './create-produit/create-produit.component';
import { ProduitsComponent } from './produits/produits.component';
import { HabitComponent } from './habit/habit.component';
import { ChaussureComponent } from './chaussure/chaussure.component';
import { SacComponent } from './sac/sac.component';
import { EnregistrerComponent } from './enregistrer/enregistrer.component';



const routes: Routes = [
    { path: 'contacts', component: ContatsComponent },
    { path: 'create-nouveau-contact', component: CreateContactComponent },
    { path: 'ajouter-nouveau-produit', component: CreateProduitComponent },
    { path: 'liste-produits', component: ProduitsComponent },
    { path: 'habit-homme', component: HabitComponent },
    { path: 'chaussure-homme', component: ChaussureComponent },
    { path: 'sac-homme', component: SacComponent },
    { path: 'habit-femme', component: HabitComponent },
    { path: 'chaussure-femme', component: ChaussureComponent },
    { path: 'sac-femme', component: SacComponent },
    { path: 'habit-enfant', component: HabitComponent },
    { path: 'chaussure-enfant', component: ChaussureComponent },
    { path: 'sac-enfant', component: SacComponent },
    { path: 'enregister', component: EnregistrerComponent },
    { path: '', redirectTo: 'liste-produits', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
