import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { MDBBootstrapModule } from 'angular-bootstrap-md';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContatsComponent } from './contats/contats.component';
import { CreateContactComponent } from './create-contact/create-contact.component';
import { CreateProduitComponent } from './create-produit/create-produit.component';
import { ProduitsComponent } from './produits/produits.component';
import { MenuComponent } from './menu/menu.component';
import { HabitComponent } from './habit/habit.component';
import { ChaussureComponent } from './chaussure/chaussure.component';
import { SacComponent } from './sac/sac.component';
import { FooterComponent } from './footer/footer.component';
import { EnregistrerComponent } from './enregistrer/enregistrer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
    declarations: [
        AppComponent,
        ContatsComponent,
        CreateContactComponent,
        CreateProduitComponent,
        ProduitsComponent,
        MenuComponent,
        HabitComponent,
        ChaussureComponent,
        SacComponent,
        FooterComponent,
        EnregistrerComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        MDBBootstrapModule.forRoot(),
        HttpClientModule,
        BrowserAnimationsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
