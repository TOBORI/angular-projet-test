import { Component, OnInit } from '@angular/core';
import { APIServiceService } from '../apiservice.service';

@Component({
    selector: 'app-contats',
    templateUrl: './contats.component.html',
    styleUrls: ['./contats.component.scss']
})
export class ContatsComponent implements OnInit {

    editField: string;
    // contacts: any;
    public contacts: Array<object> = [];

    awaitingPersonList: Array<any> = [
        {
            'id': 11,
            'name': 'Leanne Graham',
            'username': 'Bret',
            'email': 'Sincere@april.biz',
            'address': {
                'street': 'Kulas Light',
                'suite': 'Apt. 556',
                'city': 'Gwenborough',
                'zipcode': '92998-3874',
                'geo': {
                    'lat': '-37.3159',
                    'lng': '81.1496'
                }
            },
            'phone': '1-770-736-8031 x56442',
            'website': 'hildegard.org',
            'company': {
                'name': 'Romaguera-Crona',
                'catchPhrase': 'Multi-layered client-server neural-net',
                'bs': 'harness real-time e-markets'
            }
        },
        {
            'id': 12,
            'name': 'Ervin Howell',
            'username': 'Antonette',
            'email': 'Shanna@melissa.tv',
            'address': {
                'street': 'Victor Plains',
                'suite': 'Suite 879',
                'city': 'Wisokyburgh',
                'zipcode': '90566-7771',
                'geo': {
                    'lat': '-43.9509',
                    'lng': '-34.4618'
                }
            },
            'phone': '010-692-6593 x09125',
            'website': 'anastasia.net',
            'company': {
                'name': 'Deckow-Crist',
                'catchPhrase': 'Proactive didactic contingency',
                'bs': 'synergize scalable supply-chains'
            }
        },
        {
            'id': 13,
            'name': 'Clementine Bauch',
            'username': 'Samantha',
            'email': 'Nathan@yesenia.net',
            'address': {
                'street': 'Douglas Extension',
                'suite': 'Suite 847',
                'city': 'McKenziehaven',
                'zipcode': '59590-4157',
                'geo': {
                    'lat': '-68.6102',
                    'lng': '-47.0653'
                }
            },
            'phone': '1-463-123-4447',
            'website': 'ramiro.info',
            'company': {
                'name': 'Romaguera-Jacobson',
                'catchPhrase': 'Face to face bifurcated interface',
                'bs': 'e-enable strategic applications'
            }
        },
        {
            'id': 14,
            'name': 'Patricia Lebsack',
            'username': 'Karianne',
            'email': 'Julianne.OConner@kory.org',
            'address': {
                'street': 'Hoeger Mall',
                'suite': 'Apt. 692',
                'city': 'South Elvis',
                'zipcode': '53919-4257',
                'geo': {
                    'lat': '29.4572',
                    'lng': '-164.2990'
                }
            },
            'phone': '493-170-9623 x156',
            'website': 'kale.biz',
            'company': {
                'name': 'Robel-Corkery',
                'catchPhrase': 'Multi-tiered zero tolerance productivity',
                'bs': 'transition cutting-edge web services'
            }
        },
        {
            'id': 15,
            'name': 'Chelsey Dietrich',
            'username': 'Kamren',
            'email': 'Lucio_Hettinger@annie.ca',
            'address': {
                'street': 'Skiles Walks',
                'suite': 'Suite 351',
                'city': 'Roscoeview',
                'zipcode': '33263',
                'geo': {
                    'lat': '-31.8129',
                    'lng': '62.5342'
                }
            },
            'phone': '(254)954-1289',
            'website': 'demarco.info',
            'company': {
                'name': 'Keebler LLC',
                'catchPhrase': 'User-centric fault-tolerant solution',
                'bs': 'revolutionize end-to-end systems'
            }
        },
        {
            'id': 16,
            'name': 'Mrs. Dennis Schulist',
            'username': 'Leopoldo_Corkery',
            'email': 'Karley_Dach@jasper.info',
            'address': {
                'street': 'Norberto Crossing',
                'suite': 'Apt. 950',
                'city': 'South Christy',
                'zipcode': '23505-1337',
                'geo': {
                    'lat': '-71.4197',
                    'lng': '71.7478'
                }
            },
            'phone': '1-477-935-8478 x6430',
            'website': 'ola.org',
            'company': {
                'name': 'Considine-Lockman',
                'catchPhrase': 'Synchronised bottom-line interface',
                'bs': 'e-enable innovative applications'
            }
        },
        {
            'id': 17,
            'name': 'Kurtis Weissnat',
            'username': 'Elwyn.Skiles',
            'email': 'Telly.Hoeger@billy.biz',
            'address': {
                'street': 'Rex Trail',
                'suite': 'Suite 280',
                'city': 'Howemouth',
                'zipcode': '58804-1099',
                'geo': {
                    'lat': '24.8918',
                    'lng': '21.8984'
                }
            },
            'phone': '210.067.6132',
            'website': 'elvis.io',
            'company': {
                'name': 'Johns Group',
                'catchPhrase': 'Configurable multimedia task-force',
                'bs': 'generate enterprise e-tailers'
            }
        },
        {
            'id': 18,
            'name': 'Nicholas Runolfsdottir V',
            'username': 'Maxime_Nienow',
            'email': 'Sherwood@rosamond.me',
            'address': {
                'street': 'Ellsworth Summit',
                'suite': 'Suite 729',
                'city': 'Aliyaview',
                'zipcode': '45169',
                'geo': {
                    'lat': '-14.3990',
                    'lng': '-120.7677'
                }
            },
            'phone': '586.493.6943 x140',
            'website': 'jacynthe.com',
            'company': {
                'name': 'Abernathy Group',
                'catchPhrase': 'Implemented secondary concept',
                'bs': 'e-enable extensible e-tailers'
            }
        },
        {
            'id': 19,
            'name': 'Glenna Reichert',
            'username': 'Delphine',
            'email': 'Chaim_McDermott@dana.io',
            'address': {
                'street': 'Dayna Park',
                'suite': 'Suite 449',
                'city': 'Bartholomebury',
                'zipcode': '76495-3109',
                'geo': {
                    'lat': '24.6463',
                    'lng': '-168.8889'
                }
            },
            'phone': '(775)976-6794 x41206',
            'website': 'conrad.com',
            'company': {
                'name': 'Yost and Sons',
                'catchPhrase': 'Switchable contextually-based project',
                'bs': 'aggregate real-time technologies'
            }
        },
        {
            'id': 20,
            'name': 'Clementina DuBuque',
            'username': 'Moriah.Stanton',
            'email': 'Rey.Padberg@karina.biz',
            'address': {
                'street': 'Kattie Turnpike',
                'suite': 'Suite 198',
                'city': 'Lebsackbury',
                'zipcode': '31428-2261',
                'geo': {
                    'lat': '-38.2386',
                    'lng': '57.2232'
                }
            },
            'phone': '024-648-3804',
            'website': 'ambrose.net',
            'company': {
                'name': 'Hoeger LLC',
                'catchPhrase': 'Centralized empowering task-force',
                'bs': 'target end-to-end models'
            }
        }
    ];
    headElements = ['Id ', 'Nom', 'Prenom', 'Tel', 'Email', 'Adresse', 'Compagnie', 'Trie', 'Remove'];
    constructor(public apiService: APIServiceService) { }

    ngOnInit() {
        this.getContacts();
    }

    public getContacts() {
        this.apiService.
            getContacts().
            subscribe((data: Array<object>) => {
                this.contacts = data;
                console.log(data);
            });
    }
    public add() {
        if (this.awaitingPersonList.length > 0) {
            const person = this.awaitingPersonList[0];
            this.contacts.push(person);
            this.awaitingPersonList.splice(0, 1);
        }
    }


    updateList(id: number, property: string, event: any) {
        const editField = event.target.textContent;
        this.contacts[id][property] = editField;
    }

    remove(id: any) {
        this.awaitingPersonList.push(this.contacts[id]);
        this.contacts.splice(id, 1);
    }


    changeValue(id: number, property: string, event: any) {
        this.editField = event.target.textContent;
    }


    sortTable() {
        let table, rows, switching, i, x, y, shouldSwitch;
        table = document.getElementsByClassName('z-depth-1');
        switching = true;
        /*Make a loop that will continue until
        no switching has been done:*/
        while (switching) {

            switching = false;
            rows = table.rows;
            /*Loop through all table rows (except the
            first, which contains table headers):*/
            for (i = 1; i < (rows.length - 1); i++) {

                shouldSwitch = false;
                /*Get the two elements you want to compare,
                one from current row and one from the next:*/
                x = rows[i].getElementsByTagName('TD')[0];
                y = rows[i + 1].getElementsByTagName('TD')[0];
                /*check if the two rows should switch place:*/
                if (Number(x.innerHTML) > Number(y.innerHTML)) {
                    /*if so, mark as a switch and break the loop:*/
                    shouldSwitch = true;
                    break;
                }
            }
            if (shouldSwitch) {
                /*If a switch has been marked, make the switch
                and mark that a switch has been done:*/
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
            }
        }
    }
}
